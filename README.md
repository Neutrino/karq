# ![Karq icon](./karq/assets/karq.png){width=50 height=50} Karq (**K**arq **a**sks **r**andom **q**uestions)

## Français
**Karq** est un petit logiciel qui permet de poser des questions au hasard parmi celles que vous avez enregistrées dans la base de données.

### Fonctionnalités

- Ajouter une question (une par une pour le moment, l'import masssif sera une des futures fonctionnalités)
- Question sous forme de texte avec ou sans image
- Survole des miniatures d'images pour les agrandir 
- Visualiser les questions présentes dans la base de données
- Rechercher parmi vos questions enregistrées
- Supprimer des questions (une par une ou toutes d'un coup)
- Tirer au sort des questions sans redondance (possibilié d'en tirer entre 0 et 20, si le nombre demandé est supérieur au nombre de questions présentes dans la base de données, ne fait rien)
- Thème sombre / Thème clair
- Zoomer, dézoomer l'interface
- Nombreux raccourcis claviers (voir section ci-dessous)

### Raccourcis
- `Ctrl + R` tire au sort (équivalent au bouton dédié)
- `Ctrl + F` (quand la liste des questions est déroulée) permet d'entrer dans la zone de recherche des questions
- `Ctrl + J` pour joindre une image à la question
- `Ctrl + +` pour zoomer
- `Ctrl + -` pour dézoomer
- `Ctrl + 0` pour réinitialiser le zoom
- `Ctrl + Q` pour quitter l'application
- `Ctrl + I` pour afficher la popup **À propos**

### Pour qui ?

Ce logiciel a été conçu pour les enseignants qui souhaitent poser des questions sur ce qu'ils ont vu en cours avec leurs élèves lors des séances précédentes pour les faire réviser.

### Captures d'écrans

Des captures d'écran sont disponibles dans [karq/assets/screenshots](./karq/assets/screenshots/).

### Installation

- Windows
  - Télécharger le fichier `karq-v103-windows.exe` depuis la [page de téléchargements](http://e.pc.cd/BJRy6alK). (Pour l'historique des versions, voir : [page release](https://framagit.org/Neutrino/karq/-/releases) (*release* : publication).)
  - Le fichier `karq-v103-windows.exe` est l'exécutable qui permet de lancer le logiciel.
  - Placez-le dans le dossier de votre choix. Lors du premier lancement, il créera un fichier `database.db` qui contiendra toutes les questions que vous enregistrerez (et bien plus lors des future mises à jour), vous pouvez le copier ailleurs de temps en temps comme copie de sauvegarde en cas de problème. Le fichier `database.db` doit *toujours* être dans le même dossier que l'exécutable `karq-v103-windows.exe` !
- Linux
  - Télécharger le fichier `karq-v103-linux` depuis la [page de téléchargements](http://e.pc.cd/BJRy6alK). (Pour l'historique des versions, voir : [page release](https://framagit.org/Neutrino/karq/-/releases) (*release* : publication).)
  - Le fichier `karq-v103-linux` est l'exécutable qui permet de lancer le logiciel.
  - Placez-le dans le dossier de votre choix. Lors du premier lancement, il créera un fichier `database.db` qui contiendra toutes les questions que vous enregistrerez (et bien plus lors des future mises à jour), vous pouvez le copier ailleurs de temps en temps comme copie de sauvegarde en cas de problème. Le fichier `database.db` doit *toujours* être dans le même dossier que l'exécutable `karq-v103-linux` !
- Mac OS et autres systèmes d'exploitation
  - Il n'y a pas d'exécutable fourni ni pour Mac OS ni pour les autres systèmes, toutefois vous pouvez vous référer à la section ci-dessous *Compilez-le par vous-même !* ou chercher de l'aide sur Internet pour cross-compiler avec l'utilitaire `cargo` si le système visé n'est pas compatible avec le compilateur Rust.

### Compilez-le par vous-même !

Si vous souhaitez compilez Karq par vous-même, voici comment faire :
- Cloner ce dépôt
- Déplacez-vous dans le dossier `karq`
- Vérifiez que Rust a bien été installé avec`rustup` ou au moins que vous pouvez utiliser `cargo`
- Exécutez `cargo build --release` depuis votre ligne de commande (Terminal)
- L'exécutable devrait être dans le dossier `target/release`
- Bravo ! Vous avez réussi !

## English
**Karq** is a small software that allows to ask random questions from those stored in the database.

⚠️ Please, note that Karq is **not** fully translated in English.

### Features

- Add a question (one by one for now, massive import will be one of the next features)
- Questions are text with or without picture
- Hover picture thumbnails to zoom them in
- Show questions that are in the database
- Search in your stored questions
- Delete questions (one by one, or everyone)
- Draw randomly without redundance (can pick up from 0 to 20 questions, if you ask for more than there is questions in the database, it won't do anything)
- Dark / Light modes
- Zoom in, zoom out interface
- Many keyboard shortcuts (see following section)

### Raccourcis
- `Ctrl + R` draw randomly
- `Ctrl + F` give focus on text field to search in your questions
- `Ctrl + J` add picture to question
- `Ctrl + +` zoom in
- `Ctrl + -` zoom out
- `Ctrl + 0` reset zoom
- `Ctrl + Q` leave application
- `Ctrl + I` show **About** popup

### Is it for you ?

This software was made for teachers who want to ask questions about what they talked about in their precedent classes with their students to make them practice.

### Screenshots

Screenshots are available here : [karq/assets/screenshots](./karq/assets/screenshots/).

### Installation

- Windows
  - Download `karq-v103-windows.exe` file (see [release page](https://framagit.org/Neutrino/karq/-/releases)).
  - `karq-v103-windows.exe` file is the one which will launch the software.
  - Put it in any directory of your choice. At first start, it will make `database.db` file whose goal is to store every questions you'll add (and a lot more in next updates), you can copy it somewhere when you want as a backup in case of issues. `database.db` file must *always* be in the same directory with `karq-v103-windows.exe`!
- Linux
  - Download `karq-v103-linux` file (see [release page](https://framagit.org/Neutrino/karq/-/releases)).
  - `karq-v103-linux` file is the one which will launch the software.
  - Put it in any directory of your choice. At first start, it will make `database.db` file whose goal is to store every questions you'll add (and a lot more in next updates), you can copy it somewhere when you want as a backup in case of issues. `database.db` file must *always* be in the same directory with `karq-v103-linux`!
- Mac OS and other OS
  - There isn't binary file for Mac OS neither for other OS, however you may read next section *Build it by yourself!* or look around for cross-compilation with `cargo` tool if Rust compilator isn't available for your target system.

### Build it by yourself!

If you want to build it by yourself, here is how to do it :
- Clone this repo
- Go in the `karq` directory
- Make sure you've installed Rust with `rustup` or at least that you can use `cargo`
- Run `cargo build --release` from command line
- Binary file should be in `target/release`
- Yeah ! You did it !