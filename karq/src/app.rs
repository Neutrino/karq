use crate::{
    db_managing::{self, connect_db},
    random_tasker,
};

const LICENSE: &str = "Copyright © 2024 <Neutrino HBM>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.";

// const LICENSE: &str = include_str!("../../LICENSE");

//Shortcut definitions :
const KB_MODIFIER_CTRL: egui::Modifiers = egui::Modifiers {
    alt: false,
    ctrl: false,
    shift: false,
    mac_cmd: false,
    command: true,
};
const RANDOM_SORT_SHORTCUT: egui::KeyboardShortcut =
    egui::KeyboardShortcut::new(KB_MODIFIER_CTRL, egui::Key::R);
const DELETE_DB_SHORTCUT: egui::KeyboardShortcut =
    egui::KeyboardShortcut::new(KB_MODIFIER_CTRL, egui::Key::Delete);
const QUIT_SHORTCUT: egui::KeyboardShortcut =
    egui::KeyboardShortcut::new(KB_MODIFIER_CTRL, egui::Key::Q);
const FOCUS_ON_SEARCH_SHORTCUT: egui::KeyboardShortcut =
    egui::KeyboardShortcut::new(KB_MODIFIER_CTRL, egui::Key::F);
const JOIN_IMAGE_SHORTCUT: egui::KeyboardShortcut =
    egui::KeyboardShortcut::new(KB_MODIFIER_CTRL, egui::Key::J);
const ABOUT_WINDOW_SHORTCUT: egui::KeyboardShortcut =
    egui::KeyboardShortcut::new(KB_MODIFIER_CTRL, egui::Key::I);

// Function to show image if there is one
fn add_image(e: &db_managing::DbElement, ui: &mut egui::Ui) {
    if e.image {
        let storage_dir = db_managing::get_storage_dir();
        let f = storage_dir.join("images").join(format!("{}.png", e.id));
        let p = "file://".to_string() + f.display().to_string().as_str();
        let img = egui::Image::from_uri(p);
        let r = ui.add(img.clone());
        r.on_hover_ui(|ui| {
            ui.add(img);
        })
        .on_hover_cursor(egui::CursorIcon::Help);
    }
}

// Function to show "Options" menu
fn menu(ctx: &egui::Context, app: &mut KarqApp) {
    egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
        egui::menu::bar(ui, |ui| {
            // NOTE: no File->Quit on web pages!
            let is_web = cfg!(target_arch = "wasm32");
            if !is_web {
                ui.menu_button("Options", |ui| {
                    egui::widgets::global_dark_light_mode_buttons(ui);
                    //Boutons de zoom
                    let zoom_in_button = egui::Button::new("Zoomer").shortcut_text("Ctrl++");
                    if ui.add(zoom_in_button).clicked() {
                        egui::gui_zoom::zoom_in(ctx);
                    }
                    let reset_zoom_button =
                        egui::Button::new("Rétablir le zoom").shortcut_text("Ctrl+0");
                    if ui.add(reset_zoom_button).clicked() {
                        ctx.set_zoom_factor(1.5);
                    }
                    let zoom_out_button = egui::Button::new("Dézoomer").shortcut_text("Ctrl+-");
                    if ui.add(zoom_out_button).clicked() {
                        egui::gui_zoom::zoom_out(ctx);
                    }

                    //Divers boutons
                    let delete_db_button = egui::Button::new("Supprimer la base de données")
                        .shortcut_text("Ctrl+Suppr");
                    if ui.add(delete_db_button).clicked() {
                        app.popup_reset_db_open ^= true; //toggle bool
                        ui.close_menu();
                    }
                    let about_button = egui::Button::new("À propos").shortcut_text("Ctrl+I");
                    if ui.add(about_button).clicked() {
                        app.popup_about_open ^= true;
                        ui.close_menu();
                    }
                    let quit_button = egui::Button::new("Quitter").shortcut_text("Ctrl+Q");
                    if ui.add(quit_button).clicked() {
                        ctx.send_viewport_cmd(egui::ViewportCommand::Close);
                    }
                });
                ui.add_space(16.0);
            }
        });
    });
}

fn check_shortcuts(ctx: &egui::Context, app: &mut KarqApp) {
    //Check if user request to reset zoom
    if ctx.input_mut(|i| i.consume_shortcut(&egui::gui_zoom::kb_shortcuts::ZOOM_RESET)) {
        ctx.set_zoom_factor(1.5);
    };

    //Check if user request to delete database with shortcut
    if ctx.input_mut(|i| i.consume_shortcut(&DELETE_DB_SHORTCUT)) {
        app.popup_reset_db_open ^= true; //toggle bool
    };

    //Check if user request to quit app
    if ctx.input_mut(|i| i.consume_shortcut(&QUIT_SHORTCUT)) {
        ctx.send_viewport_cmd(egui::ViewportCommand::Close);
    };

    //Check if user request about window
    if ctx.input_mut(|i| i.consume_shortcut(&ABOUT_WINDOW_SHORTCUT)) {
        app.popup_about_open ^= true;
    };
}

//Function to show "About" window
fn show_about(ui: &mut egui::Ui) {
    ui.vertical_centered_justified(|ui| {
        ui.spacing_mut().item_spacing.x = 0.0;
        ui.horizontal(|ui| {
            ui.hyperlink_to("Karq ", "https://framagit.org/Neutrino/karq");
            ui.label("was made by ");
            ui.hyperlink_to("Neutrino (HBM) ", "https://framagit.org/Neutrino");
        });
        ui.separator();
        ui.horizontal(|ui| {
            ui.label("Powered by ");
            ui.hyperlink_to("Rust ", "https://rust-lang.org");
            ui.label("with egui and eframe. ");
        });
        ui.separator();
        ui.horizontal(|ui| {
            ui.label("This software is released under the ");
            ui.hyperlink_to("MIT License", "https://mit-license.org/");
        });
        ui.label(LICENSE);
        ui.separator();
        egui::warn_if_debug_build(ui);
    });
}

fn reload_random_questions(app: &mut KarqApp) {
    let questions = random_tasker::select_questions(
        db_managing::read_questions_in_database(&app.db_connection),
        app.random_questions_qty,
    );
    app.random_questions = questions;
}

pub struct KarqApp {
    new_question: String,
    random_questions: Vec<db_managing::DbElement>,
    random_questions_qty: u64,
    popup_reset_db_open: bool,
    popup_about_open: bool,
    search_in_questions: String,
    search_filter: bool,
    db_connection: rusqlite::Connection,
    selected_image: Option<std::path::PathBuf>,
}

impl Default for KarqApp {
    fn default() -> Self {
        Self {
            // Example stuff:
            new_question: "".to_string(),
            random_questions: Vec::new(),
            random_questions_qty: 10,
            popup_reset_db_open: false,
            popup_about_open: false,
            search_in_questions: String::new(),
            search_filter: false,
            db_connection: connect_db(),
            selected_image: None,
        }
    }
}

impl KarqApp {
    // Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.
        let settings = egui::Visuals {
            hyperlink_color: egui::Color32::from_rgb(240, 190, 53),
            window_rounding: egui::Rounding {
                nw: 10.,
                ne: 10.,
                sw: 10.,
                se: 10.,
            },
            ..egui::Visuals::default()
        };
        cc.egui_ctx.set_visuals(settings);
        //Set font
        let mut fonts = egui::FontDefinitions::default();
        // Install Biolinum font:
        fonts.font_data.insert(
            "Biolinum".to_owned(),
            egui::FontData::from_static(include_bytes!(
                "../assets/fonts/LinuxLibertine/LinBiolinum_R.otf"
            )),
        );
        // Put Biolinum first (highest priority):
        fonts
            .families
            .get_mut(&egui::FontFamily::Proportional)
            .unwrap()
            .insert(0, "Biolinum".to_owned());
        // Put Biolinum as last fallback for monospace:
        fonts
            .families
            .get_mut(&egui::FontFamily::Monospace)
            .unwrap()
            .push("Biolinum".to_owned());
        cc.egui_ctx.set_fonts(fonts);
        cc.egui_ctx.set_zoom_factor(1.5);
        // cc.egui_ctx.set_style(style);
        Default::default()
    }
}

impl eframe::App for KarqApp {
    /// Called each time the UI needs repainting, which may be many times per second.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        //Keep egui settings on:
        let visuals = ctx.style().visuals.clone();
        egui_extras::install_image_loaders(ctx);
        let hyperlink_color = if visuals.dark_mode {
            egui::Color32::from_rgb(240, 190, 53)
        } else {
            egui::Color32::from_rgb(170, 95, 29)
        };
        let settings = egui::Visuals {
            hyperlink_color,
            ..visuals
        };
        ctx.set_visuals(settings);

        //Show menu
        menu(ctx, self);

        //Check if user call shortcuts
        check_shortcuts(ctx, self);

        egui::CentralPanel::default().show(ctx, |ui| {
            // The central panel the region left after adding TopPanel's and SidePanel's
            ui.heading("Karq");

            ui.separator();

            ui.vertical(|ui| {
                ui.horizontal(|ui| {
                    ui.label("Ajouter une question : ");
                    let question_editing = ui.add(
                        egui::TextEdit::singleline(&mut self.new_question)
                            .hint_text("Pourquoi le ciel est bleu ?"),
                    );
                    let join_image = ui
                        .add(egui::Button::new("Joindre une image"))
                        .on_hover_text("Ctrl+J");
                    if join_image.clicked()
                        || ctx.input_mut(|i| i.consume_shortcut(&JOIN_IMAGE_SHORTCUT))
                    {
                        let default_path = db_managing::get_last_file_folder(&self.db_connection);
                        let exts = Vec::from(["png", "jpg", "jpeg", "bmp"]);
                        let mut fd = rfd::FileDialog::new()
                            .set_title("Choisir une image")
                            .add_filter("images", &exts);
                        match default_path {
                            None => (),
                            Some(p) => fd = fd.set_directory(p),
                        }
                        if let Some(p) = fd.pick_file() {
                            db_managing::set_last_file_folder(
                                &self.db_connection,
                                &p.parent().unwrap().display().to_string(),
                            );
                            self.selected_image = Some(p);
                        }
                    }
                    match self.selected_image.clone() {
                        None => (),
                        Some(p) => {
                            let p = "file://".to_string() + p.display().to_string().as_str();
                            let img = egui::Image::from_uri(p);
                            ui.add(img);
                        }
                    }
                    if question_editing.lost_focus()
                        && ui.input(|i| i.key_pressed(egui::Key::Enter))
                    {
                        let question = self.new_question.clone();
                        db_managing::add_new_question_in_db(
                            question,
                            &self.selected_image,
                            &self.db_connection,
                        );
                        self.new_question = "".to_string();
                        self.selected_image = None;
                        question_editing.request_focus();
                    }
                });
                if ui.button("Ajouter !").clicked() {
                    let question = self.new_question.clone();
                    db_managing::add_new_question_in_db(
                        question,
                        &self.selected_image,
                        &self.db_connection,
                    );
                    self.selected_image = None;
                    self.new_question = "".to_string();
                }

                ui.horizontal(|ui| {
                    ui.label("Tirer des questions au hasard : ");
                    let slider = egui::Slider::new(&mut self.random_questions_qty, 0..=20);
                    let slider = slider.integer();
                    ui.add(slider);
                });
                if ui
                    .button("Tirer au sort !")
                    .on_hover_text("Ctrl+R")
                    .clicked()
                    || ctx.input_mut(|i| i.consume_shortcut(&RANDOM_SORT_SHORTCUT))
                {
                    reload_random_questions(self);
                    // let questions = db_managing::read_questions_in_database(&self.db_connection);
                    // self.random_questions =
                    // random_tasker::select_questions(questions, self.random_questions_qty);
                }
            });
            ui.separator();
            ui.collapsing("Lister mes questions", |ui| {
                ui.label("Rechercher une question : ")
                    .on_hover_text("Ctrl+F");
                let search_query = ui.add(
                    egui::TextEdit::singleline(&mut self.search_in_questions)
                        .hint_text("Recherche..."),
                );
                //Check if user request to search
                if ctx.input_mut(|i| i.consume_shortcut(&FOCUS_ON_SEARCH_SHORTCUT)) {
                    search_query.request_focus()
                };
                if search_query.changed() {
                    self.search_filter = !self.search_in_questions.is_empty();
                }
                egui::ScrollArea::both()
                    .max_height(f32::INFINITY)
                    .max_width(f32::INFINITY)
                    .auto_shrink(false)
                    .show(ui, |ui| {
                        let query = self.search_in_questions.clone();
                        let db: Vec<db_managing::DbElement> = {
                            if self.search_filter {
                                db_managing::search_questions_in_database(
                                    query,
                                    &self.db_connection,
                                )
                            } else {
                                db_managing::read_questions_in_database(&self.db_connection)
                            }
                        };
                        let mut i: u64 = 1;
                        for e in db {
                            ui.horizontal(|ui| {
                                ui.label(i.to_string());
                                i += 1;
                                if ui.button("Supprimer").clicked() {
                                    db_managing::remove_question(e.id, &self.db_connection);
                                }
                                let sep = egui::Separator::default();
                                let sep = sep.vertical();
                                ui.add(sep);
                                ui.label(e.question.clone());
                                add_image(&e, ui);
                            });
                        }
                    });
            });
            ui.separator();

            let mut popup_is_open = self.popup_reset_db_open;
            //popup to ask confirmation for deleting database
            if self.popup_reset_db_open {
                egui::Window::new("Supprimer la base de données ?")
                    .anchor(egui::Align2::CENTER_CENTER, [0., 0.])
                    .title_bar(true)
                    .open(&mut popup_is_open)
                    .show(ctx, |ui| {
                        let yes = ui.button("Oui, supprimer la base de données");
                        let no = ui.button("Non, annuler");
                        if yes.clicked() {
                            self.db_connection = db_managing::delete_db(&self.db_connection);
                            self.popup_reset_db_open ^= true;
                        }
                        if no.clicked() {
                            self.popup_reset_db_open ^= true;
                        }
                    });
            };
            self.popup_reset_db_open = self.popup_reset_db_open && popup_is_open;

            let about_window = egui::Window::new("À propos");
            about_window
                .anchor(egui::Align2::CENTER_CENTER, [0., 0.])
                .interactable(true)
                .title_bar(true)
                .hscroll(true)
                .vscroll(true)
                .open(&mut self.popup_about_open)
                .show(ctx, show_about);

            ui.heading("Tirage au sort");
            egui::ScrollArea::both()
                .max_height(f32::INFINITY)
                .max_width(f32::INFINITY)
                .auto_shrink(false)
                .show(ui, |ui| {
                    let mut i = 1;
                    for e in &self.random_questions {
                        ui.horizontal(|ui| {
                            ui.label(i.to_string());
                            let sep = egui::Separator::default();
                            let sep = sep.vertical();
                            ui.add(sep);
                            i += 1;
                            ui.label(e.question.clone());
                            add_image(e, ui);
                        });
                    }
                });
        });
    }
}
