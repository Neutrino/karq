#![warn(clippy::all, rust_2018_idioms)]

mod app;
mod db_managing;
mod random_tasker;
pub use app::KarqApp;
