use log::debug;
use rusqlite::{self, Connection};
use std::fs;
use std::path::PathBuf;

pub struct DbElement {
    pub id: i64,
    pub question: String,
    pub image: bool,
}

pub fn get_storage_dir() -> PathBuf {
    let dir = match dirs::data_dir() {
        None => panic!("Unsupported OS"),
        Some(p) => p.join("Karq"),
    };

    // Ensure the directory exists
    if !dir.exists() {
        std::fs::create_dir_all(&dir).expect("Failed to create storage directory");
    }
    if !dir.join("images").exists() {
        std::fs::create_dir(dir.join("images")).expect("Failed to create image storage directory");
    }

    dir
}

pub fn connect_db() -> Connection {
    let storage_path = get_storage_dir();
    rusqlite::Connection::open(storage_path.join("database.db")).unwrap()
}

pub fn search_questions_in_database(txt: String, db_connection: &Connection) -> Vec<DbElement> {
    let mut db: Vec<DbElement> = Vec::new();

    let query = "SELECT id, question, image FROM questions WHERE question LIKE ?1;";
    let mut statement = db_connection.prepare(query).unwrap();

    let rows = statement
        .query_map(rusqlite::params![format!("%{}%", txt)], |row| {
            Ok(DbElement {
                id: row.get(0)?,
                question: row.get(1)?,
                image: row.get(2)?,
            })
        })
        .unwrap();

    for row in rows {
        db.push(row.unwrap());
    }

    db
}

pub fn read_questions_in_database(db_connection: &Connection) -> Vec<DbElement> {
    let mut db: Vec<DbElement> = Vec::new();
    let query = "SELECT id, question, image FROM questions;";

    let mut statement = db_connection.prepare(query).unwrap();
    let rows = statement
        .query_map([], |row| {
            Ok(DbElement {
                id: row.get(0)?,
                question: row.get(1)?,
                image: row.get(2)?,
            })
        })
        .unwrap();

    for row in rows {
        db.push(row.unwrap());
    }

    db
}

pub fn create_database(db_connection: &Connection) {
    // TABLE DES QUESTIONS
    let query = "CREATE TABLE IF NOT EXISTS questions
    (id INTEGER PRIMARY KEY AUTOINCREMENT,
    question TEXT NOT NULL,
    image INT NOT NULL);";
    db_connection
        .execute(query, [])
        .expect("ERREUR DANS LA CRÉATION DE LA BASE DE DONNÉES (table des questions)");

    // TABLE DES PARAMÈTRES (servira dans le futur)
    let query = "CREATE TABLE IF NOT EXISTS settings (id TEXT PRIMARY KEY, val TEXT);";
    db_connection
        .execute(query, [])
        .expect("ERREUR DANS LA CRÉATION DE LA BASE DE DONNÉES (table des paramètres)");
}

pub fn add_new_question_in_db(txt: String, img: &Option<PathBuf>, db_connection: &Connection) {
    //avoid adding empty string
    if txt.is_empty() {
        return;
    }
    let image: i64 = match &img {
        None => 0,
        Some(_) => 1,
    };
    let query = format!("INSERT INTO questions (question,image) VALUES(\"{txt}\",{image});");
    let query = query.as_str();
    db_connection
        .execute(query, [])
        .expect("ERREUR DANS L'AJOUT D'UNE QUESTION");
    if image == 1 {
        let id: i64 = db_connection.last_insert_rowid();
        let img = img.to_owned().unwrap();
        let destination = get_storage_dir().join("images").join(format!("{id}.png"));
        let input_img = image::open(img).expect("Can't read image");
        match input_img.save(destination) {
            Ok(_) => (),
            Err(_) => debug!("Can't save image"),
        }
        // let mut png_config = image_convert::PNGConfig::new();
        // png_config.width = 500;
        // let input = image_convert::ImageResource::from_path(img);
        // let mut output = image_convert::ImageResource::from_path(destination);
        // image_convert::to_png(&mut output, &input, &png_config).unwrap();
    }
}

pub fn remove_question(id: i64, db_connection: &Connection) {
    let query = format!("DELETE FROM questions WHERE id={id};");
    let query = query.as_str();
    db_connection
        .execute(query, [])
        .expect("ERREUR DANS LA SUPPRESSION D'UNE QUESTION");
    let f = get_storage_dir().join("images").join(format!("{id}.png"));
    match fs::remove_file(f) {
        Ok(_) => (),
        Err(error) => debug!("{error}"),
    };
}

pub fn delete_db(db_connection: &Connection) -> Connection {
    let query = "DROP TABLE IF EXISTS questions;";
    db_connection
        .execute(query, [])
        .expect("ERREUR DANS LA SUPPRESSION DE LA BASE DE DONNÉES");
    let dir = get_storage_dir().join("images");
    match fs::remove_dir_all(dir) {
        Ok(_) => (),
        Err(error) => debug!("{error}"),
    };
    create_database(db_connection);
    connect_db()
}

pub fn set_last_file_folder(db_connection: &Connection, path: &String) {
    let query = format!("INSERT INTO settings (id,val) VALUES ('last_file_folder','{}') ON CONFLICT (id) DO UPDATE SET val = '{}'",path,path);
    db_connection
        .execute(&query, [])
        .expect("Can't update settings");
}

pub fn get_last_file_folder(db_connection: &Connection) -> Option<PathBuf> {
    let query = "SELECT val FROM settings WHERE id = 'last_file_folder'";
    let res: rusqlite::Result<String> = db_connection.query_row(query, [], |row| row.get(0));
    match res {
        Ok(s) => Some(PathBuf::from(s)),
        Err(_) => None,
    }
}
