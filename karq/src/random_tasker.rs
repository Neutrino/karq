/* PREND EN PARAMÈTRES UN VECTEUR DE DbElement MUTABLES ET n LE NOMBRE D'ÉLÉMENTS À TIRER AU SORT */

use crate::db_managing;

pub fn select_questions(mut v: Vec<db_managing::DbElement>, n: u64) -> Vec<db_managing::DbElement> {
    let mut questions: Vec<db_managing::DbElement> = Vec::new();
    let len: usize = v.len();
    if n > len as u64 {
        return questions;
    }
    for i in 0..n {
        let k: usize = fastrand::usize(0..(len - i as usize));
        questions.push(v.swap_remove(k))
    }
    questions
} /*RENVOIE UN VECTEUR DE n DbElement*/
